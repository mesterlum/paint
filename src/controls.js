import Draw from './draw/draw'

export default class {
    constructor() {
        this.target = document.getElementById('controls')
        this.canvas = document.getElementById('canvas')
        this.canvasClone = this.canvas.cloneNode(true)

        this.draw = new Draw()
        this.initializeControls()
    }

    removeEvents = () => {
        //this.canvas.parentNode.innerHTML += '';
        //this.canvas.parentNode.replaceChild(this.canvasClone, this.canvas);
    }

    makeButtonControl = (name) => `<button id="control-${name}">${name}</button>`

    initializeControls = () => {
        this.getControls().map((obj) => {
            this.target.innerHTML += this.makeButtonControl(obj.name)
        })

        this.getControls().map((obj) => {
            document.getElementById(`control-${obj.name}`).addEventListener('click', (e) => {
                this.removeEvents()
                obj.action()
            })

        })

    }

    getControls = () => ([
        {
            name: 'lapiz',
            action: () => {
                this.draw.printPencil()
            }
        },
        {
            name: 'linea',
            action: () => {
                this.draw.drawLine()
            }
        },
        {
            name: 'circle',
            action: () => {
                this.draw.printCircle()
            }
        }
    ])
}
import { developmentLog } from '../utils'


class Print {

    constructor() {
        this.canvas = document.getElementById("canvas")
        this.ctx = canvas.getContext("2d")
    }

    printIndividual = element => {
        if (element.type === 1) { // Line
            this.printLine(element)
        } else if (element.type === 2) {
            this.printCircle(element)
        } else if (element.type === 3) {
            this.printTriangle(element)
        } else if (element.type === 4) {
            this.printElipse(element)
        } else if (element.type === 5) {
            this.printHexagon(element)
        } else if (element.type === 6) {
            this.printPencil(element)
        }
    }

    printPencil = element => {
        this.ctx.lineTo(element.cors[0].x, element.cors[0].y);
        this.ctx.stroke();
    }

    printLine = element => {
        this.ctx.lineWidth = 3;
        this.ctx.strokeStyle = "#f00";
        this.ctx.beginPath();
        this.ctx.moveTo(element.cors[0].x, element.cors[0].y);
        this.ctx.lineTo(element.cors[1].x, element.cors[1].y);
        this.ctx.stroke();
    }

    printCircle = element => {
        this.ctx.beginPath()
        var X = element.cors[0].x
        var Y = element.cors[0].y
        var r = 40
        this.ctx.strokeStyle = "#006400"
        this.ctx.fillStyle = "#6ab150"
        this.ctx.lineWidth = 5
        this.ctx.arc(X, Y, r, 0, 2 * Math.PI)
        this.ctx.fill()
        this.ctx.stroke()
    }

    printTriangle = element => {
        var X = element.cors[0].x
        var Y = element.cors[0].y
        var R = 140;
        // El ángulo de partida ap y el ángulo final af
        var ap = (Math.PI / 180) * 60
        var af = (Math.PI / 180) * 120
        // Las coordenadas del punto de partida en la circunferencia
        var Xap = X + R * Math.cos(ap)
        var Yap = Y + R * Math.sin(ap)
        // estilos
        this.ctx.fillStyle = "#abcdef"
        this.ctx.strokeStyle = "#1E90FF"
        this.ctx.lineWidth = 5
        // empezamos a dibujar
        this.ctx.beginPath()
        this.ctx.moveTo(X, Y)
        this.ctx.lineTo(Xap, Yap)
        this.ctx.arc(X, Y, R, ap, af)
        this.ctx.closePath()
        this.ctx.fill()
        this.ctx.stroke()
    }

    printElipse = element => {
        this.ctx.beginPath()
        this.ctx.lineWidth = 5
        this.ctx.strokeStyle = "#00f";
        let centroX = element.cors[0].x, centroY = element.cors[0].y, radioX = 100, radioY = 60, rotacion = 0, ap = 0, af = 2 * Math.PI, cR = true
        this.ctx.ellipse(centroX, centroY, radioX, radioY, rotacion, ap, af, cR)
        this.ctx.stroke()
    }

    printHexagon = element => {
        var X = element.cors[0].x
        var Y = element.cors[0].y
        var R = 100
        this.ctx.fillStyle = "#6ab150"

        var rad = (Math.PI / 180) * 60
        this.ctx.beginPath()
        for (var i = 0; i < 6; i++) {
            let x = X + R * Math.cos(rad * i)
            let y = Y + R * Math.sin(rad * i)
            this.ctx.lineTo(x, y)
        }
        this.ctx.closePath()
        this.ctx.fill()
    }

}

export default new Print()
import { developmentLog } from '../utils'
import { CanvasActions } from './actions'
import Print from './print'
import { Cap } from './cap'
import History from './history'

export default class CanvasDraw {
    constructor() {
        this.canvas = document.getElementById("canvas")
        this.ctx = canvas.getContext("2d")
    }

    drawFigure(dataForHistory) {
        CanvasActions.addClick(this.canvas, data => {
            dataForHistory.cors.push(data)
            History.push(dataForHistory)
            Print.printIndividual(dataForHistory)
            dataForHistory = {
                ...dataForHistory,
                cors: []
            }
        })
    }

    drawLine() {
        developmentLog("Draw Line")

        let numberClick = 0

        let dataForHistory = {
            cap: Cap.cap,
            type: 1,
            cors: []
        }


        CanvasActions.addClick(this.canvas, data => {
            numberClick++
            dataForHistory.cors.push(data)

            if (numberClick == 2) {
                History.push(dataForHistory)
                Print.printIndividual(dataForHistory)
                dataForHistory = {
                    ...dataForHistory,
                    cors: []
                }
                numberClick = 0

            }


        })

    }

    printCircle() {
        developmentLog("Draw Circle")

        let dataForHistory = {
            cap: Cap.cap,
            type: 2,
            cors: []
        }
        this.drawFigure(dataForHistory)
    }

    printTriangle() {
        developmentLog("Draw Triangle")

        let dataForHistory = {
            cap: Cap.cap,
            type: 3,
            cors: []
        }
        this.drawFigure(dataForHistory)
    }

    printElipse() {
        developmentLog("Draw Elipse")

        let dataForHistory = {
            cap: Cap.cap,
            type: 4,
            cors: []
        }
        this.drawFigure(dataForHistory)
    }

    printHexagon() {
        developmentLog("Draw Exagon")

        let dataForHistory = {
            cap: Cap.cap,
            type: 5,
            cors: []
        }
        this.drawFigure(dataForHistory)
    }


    oMousePos(evt) {
        var ClientRect = this.canvas.getBoundingClientRect();
        return { //objeto
            x: Math.round(evt.clientX - ClientRect.left),
            y: Math.round(evt.clientY - ClientRect.top)
        }
    }

    printPencil() {
        developmentLog("Printing with pencil")
        let dibujar = false;
        const self = this

        let dataForHistory = {
            cap: Cap.cap,
            type: 6,
            cors: []
        }

        this.canvas.addEventListener('mousedown', function (evt) {
            self.ctx.beginPath();
            dibujar = true; // ya podemos dibujar
        }, false);

        this.canvas.addEventListener("mousemove", function (evt) {
            if (dibujar) {
                var m = self.oMousePos(evt);
                dataForHistory.cors.push(m)
                History.push(dataForHistory)
                Print.printIndividual(dataForHistory)
                dataForHistory = {
                    ...dataForHistory,
                    cors: []
                }
            }
        }, false);

        this.canvas.addEventListener('mouseup', function (evt) {
            dibujar = false;
        }, false);
        this.canvas.addEventListener("mouseout", function (evt) {
            dibujar = false;
        }, false);

    }
}




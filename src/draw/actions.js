export class CanvasActions {
    static addClick(canvas, cb) {
        canvas.addEventListener('click', (e) => {
            cb({
                x: e.clientX,
                y: e.clientY
            })
        })
    }
}
var path = require('path');
var webpack = require('webpack');
module.exports = {
    mode: 'development',
    entry: './src/app.js',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'app.bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/env'],
                    plugins: ['@babel/plugin-proposal-class-properties']
                },
            }
        ]
    },
    stats: {
        colors: true
    },
    devtool: 'source-map'
};
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/app.js":
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _controls__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./controls */ "./src/controls.js");

new _controls__WEBPACK_IMPORTED_MODULE_0__["default"](); // Initialize controls

/***/ }),

/***/ "./src/controls.js":
/*!*************************!*\
  !*** ./src/controls.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _draw_draw__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./draw/draw */ "./src/draw/draw.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var _default = function _default() {
  var _this = this;

  _classCallCheck(this, _default);

  _defineProperty(this, "removeEvents", function () {//this.canvas.parentNode.innerHTML += '';
    //this.canvas.parentNode.replaceChild(this.canvasClone, this.canvas);
  });

  _defineProperty(this, "makeButtonControl", function (name) {
    return "<button id=\"control-".concat(name, "\">").concat(name, "</button>");
  });

  _defineProperty(this, "initializeControls", function () {
    _this.getControls().map(function (obj) {
      _this.target.innerHTML += _this.makeButtonControl(obj.name);
    });

    _this.getControls().map(function (obj) {
      document.getElementById("control-".concat(obj.name)).addEventListener('click', function (e) {
        _this.removeEvents();

        obj.action();
      });
    });
  });

  _defineProperty(this, "getControls", function () {
    return [{
      name: 'lapiz',
      action: function action() {
        _this.draw.printPencil();
      }
    }, {
      name: 'linea',
      action: function action() {
        _this.draw.drawLine();
      }
    }, {
      name: 'circle',
      action: function action() {
        _this.draw.printCircle();
      }
    }];
  });

  this.target = document.getElementById('controls');
  this.canvas = document.getElementById('canvas');
  this.canvasClone = this.canvas.cloneNode(true);
  this.draw = new _draw_draw__WEBPACK_IMPORTED_MODULE_0__["default"]();
  this.initializeControls();
};



/***/ }),

/***/ "./src/draw/actions.js":
/*!*****************************!*\
  !*** ./src/draw/actions.js ***!
  \*****************************/
/*! exports provided: CanvasActions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CanvasActions", function() { return CanvasActions; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var CanvasActions =
/*#__PURE__*/
function () {
  function CanvasActions() {
    _classCallCheck(this, CanvasActions);
  }

  _createClass(CanvasActions, null, [{
    key: "addClick",
    value: function addClick(canvas, cb) {
      canvas.addEventListener('click', function (e) {
        cb({
          x: e.clientX,
          y: e.clientY
        });
      });
    }
  }]);

  return CanvasActions;
}();

/***/ }),

/***/ "./src/draw/cap.js":
/*!*************************!*\
  !*** ./src/draw/cap.js ***!
  \*************************/
/*! exports provided: Cap */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cap", function() { return Cap; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Cap = function Cap() {
  _classCallCheck(this, Cap);
};

_defineProperty(Cap, "cap", 1);

/***/ }),

/***/ "./src/draw/draw.js":
/*!**************************!*\
  !*** ./src/draw/draw.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CanvasDraw; });
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils */ "./src/utils.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./actions */ "./src/draw/actions.js");
/* harmony import */ var _print__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./print */ "./src/draw/print.js");
/* harmony import */ var _cap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cap */ "./src/draw/cap.js");
/* harmony import */ var _history__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./history */ "./src/draw/history.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }







var CanvasDraw =
/*#__PURE__*/
function () {
  function CanvasDraw() {
    _classCallCheck(this, CanvasDraw);

    this.canvas = document.getElementById("canvas");
    this.ctx = canvas.getContext("2d");
  }

  _createClass(CanvasDraw, [{
    key: "drawFigure",
    value: function drawFigure(dataForHistory) {
      _actions__WEBPACK_IMPORTED_MODULE_1__["CanvasActions"].addClick(this.canvas, function (data) {
        dataForHistory.cors.push(data);
        _history__WEBPACK_IMPORTED_MODULE_4__["default"].push(dataForHistory);
        _print__WEBPACK_IMPORTED_MODULE_2__["default"].printIndividual(dataForHistory);
        dataForHistory = _objectSpread({}, dataForHistory, {
          cors: []
        });
      });
    }
  }, {
    key: "drawLine",
    value: function drawLine() {
      Object(_utils__WEBPACK_IMPORTED_MODULE_0__["developmentLog"])("Draw Line");
      var numberClick = 0;
      var dataForHistory = {
        cap: _cap__WEBPACK_IMPORTED_MODULE_3__["Cap"].cap,
        type: 1,
        cors: []
      };
      _actions__WEBPACK_IMPORTED_MODULE_1__["CanvasActions"].addClick(this.canvas, function (data) {
        numberClick++;
        dataForHistory.cors.push(data);

        if (numberClick == 2) {
          _history__WEBPACK_IMPORTED_MODULE_4__["default"].push(dataForHistory);
          _print__WEBPACK_IMPORTED_MODULE_2__["default"].printIndividual(dataForHistory);
          dataForHistory = _objectSpread({}, dataForHistory, {
            cors: []
          });
          numberClick = 0;
        }
      });
    }
  }, {
    key: "printCircle",
    value: function printCircle() {
      Object(_utils__WEBPACK_IMPORTED_MODULE_0__["developmentLog"])("Draw Circle");
      var dataForHistory = {
        cap: _cap__WEBPACK_IMPORTED_MODULE_3__["Cap"].cap,
        type: 2,
        cors: []
      };
      this.drawFigure(dataForHistory);
    }
  }, {
    key: "printTriangle",
    value: function printTriangle() {
      Object(_utils__WEBPACK_IMPORTED_MODULE_0__["developmentLog"])("Draw Triangle");
      var dataForHistory = {
        cap: _cap__WEBPACK_IMPORTED_MODULE_3__["Cap"].cap,
        type: 3,
        cors: []
      };
      this.drawFigure(dataForHistory);
    }
  }, {
    key: "printElipse",
    value: function printElipse() {
      Object(_utils__WEBPACK_IMPORTED_MODULE_0__["developmentLog"])("Draw Elipse");
      var dataForHistory = {
        cap: _cap__WEBPACK_IMPORTED_MODULE_3__["Cap"].cap,
        type: 4,
        cors: []
      };
      this.drawFigure(dataForHistory);
    }
  }, {
    key: "printHexagon",
    value: function printHexagon() {
      Object(_utils__WEBPACK_IMPORTED_MODULE_0__["developmentLog"])("Draw Exagon");
      var dataForHistory = {
        cap: _cap__WEBPACK_IMPORTED_MODULE_3__["Cap"].cap,
        type: 5,
        cors: []
      };
      this.drawFigure(dataForHistory);
    }
  }, {
    key: "oMousePos",
    value: function oMousePos(evt) {
      var ClientRect = this.canvas.getBoundingClientRect();
      return {
        //objeto
        x: Math.round(evt.clientX - ClientRect.left),
        y: Math.round(evt.clientY - ClientRect.top)
      };
    }
  }, {
    key: "printPencil",
    value: function printPencil() {
      Object(_utils__WEBPACK_IMPORTED_MODULE_0__["developmentLog"])("Printing with pencil");
      var dibujar = false;
      var self = this;
      var dataForHistory = {
        cap: _cap__WEBPACK_IMPORTED_MODULE_3__["Cap"].cap,
        type: 6,
        cors: []
      };
      this.canvas.addEventListener('mousedown', function (evt) {
        self.ctx.beginPath();
        dibujar = true; // ya podemos dibujar
      }, false);
      this.canvas.addEventListener("mousemove", function (evt) {
        if (dibujar) {
          var m = self.oMousePos(evt);
          dataForHistory.cors.push(m);
          _history__WEBPACK_IMPORTED_MODULE_4__["default"].push(dataForHistory);
          _print__WEBPACK_IMPORTED_MODULE_2__["default"].printIndividual(dataForHistory);
          dataForHistory = _objectSpread({}, dataForHistory, {
            cors: []
          });
        }
      }, false);
      this.canvas.addEventListener('mouseup', function (evt) {
        dibujar = false;
      }, false);
      this.canvas.addEventListener("mouseout", function (evt) {
        dibujar = false;
      }, false);
    }
  }]);

  return CanvasDraw;
}();



/***/ }),

/***/ "./src/draw/history.js":
/*!*****************************!*\
  !*** ./src/draw/history.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var history = new Array();
/* harmony default export */ __webpack_exports__["default"] = (history);

/***/ }),

/***/ "./src/draw/print.js":
/*!***************************!*\
  !*** ./src/draw/print.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils */ "./src/utils.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var Print = function Print() {
  var _this = this;

  _classCallCheck(this, Print);

  _defineProperty(this, "printIndividual", function (element) {
    if (element.type === 1) {
      // Line
      _this.printLine(element);
    } else if (element.type === 2) {
      _this.printCircle(element);
    } else if (element.type === 3) {
      _this.printTriangle(element);
    } else if (element.type === 4) {
      _this.printElipse(element);
    } else if (element.type === 5) {
      _this.printHexagon(element);
    } else if (element.type === 6) {
      _this.printPencil(element);
    }
  });

  _defineProperty(this, "printPencil", function (element) {
    _this.ctx.lineTo(element.cors[0].x, element.cors[0].y);

    _this.ctx.stroke();
  });

  _defineProperty(this, "printLine", function (element) {
    _this.ctx.lineWidth = 3;
    _this.ctx.strokeStyle = "#f00";

    _this.ctx.beginPath();

    _this.ctx.moveTo(element.cors[0].x, element.cors[0].y);

    _this.ctx.lineTo(element.cors[1].x, element.cors[1].y);

    _this.ctx.stroke();
  });

  _defineProperty(this, "printCircle", function (element) {
    _this.ctx.beginPath();

    var X = element.cors[0].x;
    var Y = element.cors[0].y;
    var r = 40;
    _this.ctx.strokeStyle = "#006400";
    _this.ctx.fillStyle = "#6ab150";
    _this.ctx.lineWidth = 5;

    _this.ctx.arc(X, Y, r, 0, 2 * Math.PI);

    _this.ctx.fill();

    _this.ctx.stroke();
  });

  _defineProperty(this, "printTriangle", function (element) {
    var X = element.cors[0].x;
    var Y = element.cors[0].y;
    var R = 140; // El ángulo de partida ap y el ángulo final af

    var ap = Math.PI / 180 * 60;
    var af = Math.PI / 180 * 120; // Las coordenadas del punto de partida en la circunferencia

    var Xap = X + R * Math.cos(ap);
    var Yap = Y + R * Math.sin(ap); // estilos

    _this.ctx.fillStyle = "#abcdef";
    _this.ctx.strokeStyle = "#1E90FF";
    _this.ctx.lineWidth = 5; // empezamos a dibujar

    _this.ctx.beginPath();

    _this.ctx.moveTo(X, Y);

    _this.ctx.lineTo(Xap, Yap);

    _this.ctx.arc(X, Y, R, ap, af);

    _this.ctx.closePath();

    _this.ctx.fill();

    _this.ctx.stroke();
  });

  _defineProperty(this, "printElipse", function (element) {
    _this.ctx.beginPath();

    _this.ctx.lineWidth = 5;
    _this.ctx.strokeStyle = "#00f";
    var centroX = element.cors[0].x,
        centroY = element.cors[0].y,
        radioX = 100,
        radioY = 60,
        rotacion = 0,
        ap = 0,
        af = 2 * Math.PI,
        cR = true;

    _this.ctx.ellipse(centroX, centroY, radioX, radioY, rotacion, ap, af, cR);

    _this.ctx.stroke();
  });

  _defineProperty(this, "printHexagon", function (element) {
    var X = element.cors[0].x;
    var Y = element.cors[0].y;
    var R = 100;
    _this.ctx.fillStyle = "#6ab150";
    var rad = Math.PI / 180 * 60;

    _this.ctx.beginPath();

    for (var i = 0; i < 6; i++) {
      var x = X + R * Math.cos(rad * i);
      var y = Y + R * Math.sin(rad * i);

      _this.ctx.lineTo(x, y);
    }

    _this.ctx.closePath();

    _this.ctx.fill();
  });

  this.canvas = document.getElementById("canvas");
  this.ctx = canvas.getContext("2d");
};

/* harmony default export */ __webpack_exports__["default"] = (new Print());

/***/ }),

/***/ "./src/utils.js":
/*!**********************!*\
  !*** ./src/utils.js ***!
  \**********************/
/*! exports provided: developmentLog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "developmentLog", function() { return developmentLog; });
var developmentLog = function developmentLog(message) {
  if (true) console.log(message);
};

/***/ })

/******/ });
//# sourceMappingURL=app.bundle.js.map